'use strict';

/**
 * @ngdoc function
 * @name myminiprojectApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the myminiprojectApp
 */
angular.module('myminiprojectApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
